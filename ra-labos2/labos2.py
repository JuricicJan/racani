import pyglet
import numpy as np
from pyglet.gl import *

SIZE=0.5
TIMER=0
# config = Config(sample_buffers=1, samples=8)
window = pyglet.window.Window(width=1500, height=700, caption='Particle generator', resizable=True)
texture=pyglet.image.load("ra-labos2/iskrica.tga").get_texture()
particles=[]

class Particle:
    def __init__(self, position):
        self.position=position.copy()
        self.position=self.position.astype(np.float64)
        self.velocity=np.random.randint(low=-3, high=3, size=3)
        self.velocity=self.velocity.astype(np.float64)
        self.lifeTime=np.random.randint(low=200, high=350)

    def update(self, dt):
        self.position+=self.velocity.astype(np.float64)*dt 
        self.lifeTime-=1

@window.event
def on_draw():
    window.clear()
    glClear(GL_COLOR_BUFFER_BIT)

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60, 700/1500, 0.05, 10000) # fovy, aspect=h/w, near, fat

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    gluLookAt(100, 0, 0, # camera
                0, 0, 0, # lookAt
                0, 1, 0) # view up 
    glPushMatrix()
    draw_particles() 
    glPopMatrix()
    glFlush()

@window.event
def on_resize(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0, width, 0, height, -1, 1)
    glMatrixMode(GL_MODELVIEW)    

def draw_particles(): 
    global texture
    global particles
    global SIZE

    glEnable(texture.target)
    glBindTexture(texture.target, texture.id)

    glEnable(GL_BLEND)
    glBlendFunc(GL_ONE, GL_ONE)

    glPushMatrix()
    for p in particles:

        matrix = (GLfloat * 16)()
        glGetFloatv(GL_MODELVIEW_MATRIX, matrix)
        matrix = list(matrix)
        CameraRight=np.array([matrix[2], matrix[6], matrix[10]]) # normala
        CameraUp = np.array([0, 1, 0])
        CameraRight = np.cross(CameraRight, CameraUp)

        v1 = p.position + CameraRight * SIZE + CameraUp * -SIZE
        v2 = p.position + CameraRight * SIZE + CameraUp * SIZE
        v3 = p.position + CameraRight * -SIZE + CameraUp * -SIZE
        v4 = p.position + CameraRight * -SIZE + CameraUp * SIZE

        glBegin(GL_QUADS)
        glTexCoord2f(0, 0)
        glVertex3f(v3[0], v3[1], v3[2])
        glTexCoord2f(1, 0)
        glVertex3f(v4[0], v4[1], v4[2])
        glTexCoord2f(1, 1)
        glVertex3f(v2[0], v2[1], v2[2])
        glTexCoord2f(0, 1)
        glVertex3f(v1[0], v1[1], v1[2])

        glEnd()
    glDisable(GL_BLEND)
    glPopMatrix()
    glDisable(texture.target)    

def update_particles(time): 
    global particles
    global TIMER

    for p in particles:
        p.update(time)
        if(p.lifeTime<=0):
            particles.remove(p)
    if(TIMER%100==0):
        TIMER=0
        create_particles()
        create_particles2()

def create_particles():
    global particles

    for i in range(np.random.randint(low=1, high=3)):
        p=Particle(np.array([-100, 100, 0]))
        # p=Particle(np.array([0, 100, 0]))
        particles.append(p)

def create_particles2():
    global particles

    for i in range(np.random.randint(low=1, high=3)):
        p=Particle(np.array([0, 0, 0]))
        particles.append(p)

create_particles()
pyglet.clock.schedule_interval(update_particles, 1/60)
pyglet.app.run()