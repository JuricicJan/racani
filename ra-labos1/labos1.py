import pyglet
from pyglet.gl import *
import numpy as np

config = Config(sample_buffers=1, samples=8)
window = pyglet.window.Window(height=1000, width=1000, config=config, resizable=True)
paths=["ra-labos1/kocka.obj", "ra-labos1/kontrolni-vrhovi.obj"]
X=0

### Ucitavanje tijela i kontrolnih tocki
body=pyglet.graphics.Batch()
vrhovi=[]
poligoni=[]
with open(paths[0]) as file:
    for line in file.readlines():
        if(line.startswith("v")):
            temp=line.strip().split()
            lista=[]
            for i in temp[1:]:
                lista.append(float(i))
            vrhovi.append(lista)
        elif(line.startswith("f")):
            temp=line.strip().split()
            lista=[]
            for i in temp[1:]:
                lista.append(int(i))
            poligoni.append(lista)

for p in poligoni:
    body.add(3, GL_LINE_LOOP, None,
    ("v3f", [vrhovi[p[0]-1][0], vrhovi[p[0]-1][1], vrhovi[p[0]-1][2], 
            vrhovi[p[1]-1][0], vrhovi[p[1]-1][1], vrhovi[p[1]-1][2],
            vrhovi[p[2]-1][0], vrhovi[p[2]-1][1], vrhovi[p[2]-1][2]]))


bspline=pyglet.graphics.Batch()
contVrhovi=[]
krivulja=[]
derivacija=[]
with open(paths[1]) as file:
    for line in file.readlines():
        if(line.startswith("v")):
            temp=line.strip().split()
            lista=[]
            for i in temp[1:]:
                lista.append(float(i))
            contVrhovi.append(lista)

tt=np.linspace(0, 1, 20)
for i in range(1, len(contVrhovi)-2):
    for t in tt:
        p=np.array([t**3, t**2, t, 1])        
        p=np.matmul(p, 1/6*np.array([[-1, 3, -3, 1],
                                [3, -6, 3, 0],
                                [-3, 0, 3, 0],
                                [1, 4, 1, 0]]))
        p=np.matmul(p, np.array([contVrhovi[i-1], contVrhovi[i], 
                                contVrhovi[i+1], contVrhovi[i+2]]))
        krivulja.append(p)
        p_der=np.array([3*t**2, 2*t, 1, 0])
        p_der=np.matmul(p_der, 1/6*np.array([[-1, 3, -3, 1],
                                            [3, -6, 3, 0],
                                            [-3, 0, 3, 0],
                                            [1, 4, 1, 0]]))
        p_der=np.matmul(p_der, np.array([contVrhovi[i-1], contVrhovi[i], 
                                contVrhovi[i+1], contVrhovi[i+2]]))
        derivacija.append(p_der)
bspline.add(len(krivulja), GL_LINE_STRIP, None, ("v3f", np.array(krivulja).flatten().tolist()))


@window.event
def on_draw():
    global X
    window.clear()

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(90, 1, 1, 100)
    # glTranslatef(0, 0, -80)
    # glRotatef(90, 0, 1, 0)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    glTranslatef(0, 0, -80)
    # glRotatef(90, 0, 1, 0)
    # glRotatef(90, 0, 0, 1)
    # glRotatef(90, 1, 0, 0)
    # glRotatef(-90, 1, 0, 0)
    
    
    glPushMatrix()
    glTranslatef(krivulja[X][0], krivulja[X][1], krivulja[X][2])
    glScalef(2.5,2.5,2.5)

    # norma=np.sqrt(np.sum(derivacija[X]**2))
    norma=1
    bspline.add(2, GL_LINES, None, ("v3f", [krivulja[X][0], krivulja[X][1], krivulja[X][2],
                                        krivulja[X][0]+derivacija[X][0]/norma, krivulja[X][1]+derivacija[X][1]/norma, krivulja[X][2]+derivacija[X][2]/norma]),
                                        ("c4B", [0, 255, 255, 255, 255, 255, 0, 0]))

    s=np.array([0,0,1])
    e=derivacija[X]
    os_rot=np.cross(s, e)
    angle=180/np.pi*np.arccos(np.dot(s, e)/(np.sqrt(np.sum(s**2))*np.sqrt(np.sum(e**2))))
    glRotatef(angle, os_rot[0], os_rot[1], os_rot[2])
    body.draw()

    glPopMatrix()
    bspline.draw()
    glFlush()

@window.event
def on_resize(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0, width, 0, height, -1, 1)
    glMatrixMode(GL_MODELVIEW)    

def idle(event):
    global X
    X+=1
    if(X>=len(krivulja)):
        X=0

pyglet.clock.schedule_interval(idle,1/60)
pyglet.app.run()