import pygame
from pygame.locals import *

import pymunk
from pymunk.pygame_util import *
from pymunk import Vec2d
import math

PATH = "RA-projekt-AngryBirds/images/"
DISPLAY_WIDTH = 1203
DISPLAY_HEIGHT = 600
FPS = 60


pygame.init()
window = pygame.display.set_mode((DISPLAY_WIDTH, DISPLAY_HEIGHT))
pygame.display.set_caption("Simple Angry Birds Game")
clock = pygame.time.Clock()
space = pymunk.Space()
space.gravity = 0, -100

def convert_coordinates(point):
    return int(point[0]), int(DISPLAY_HEIGHT-point[1])

def post_solve_bird_pig(arbiter, space, data):
    bird, pig = arbiter.shapes
    space.remove(pig)
    space.remove(pig.body)

    for object in Game.objects:
        if object.body == pig.body:
            Game.objects.remove(object)

    Game.score += 100

def post_solve_wood_pig(arbiter, space, data):
    wood, pig = arbiter.shapes

    if arbiter.total_impulse.length > 350:
        print(arbiter.total_impulse.length)
        space.remove(pig)
        space.remove(pig.body)

        for object in Game.objects:
            if object.body == pig.body:
                Game.objects.remove(object)

        Game.score += 50

# type: 0=wood, 1=pig, 2=bird
space.add_collision_handler(2, 1).post_solve = post_solve_bird_pig
space.add_collision_handler(0, 1).post_solve = post_solve_wood_pig


class Circle:
    def __init__(self, position, type, r=32):
        self.r = r
        mass = 20.0
        moment = pymunk.moment_for_circle(mass, 0, self.r, (0,0))
        self.body = pymunk.Body(mass, moment)
        self.body.position = position
        self.shape = pymunk.Circle(self.body, r)
        # self.shape.density = 1
        self.shape.elasticity = 0.8
        self.shape.friction = 1
        self.shape.collision_type = type
        space.add(self.body, self.shape)
        Game.objects.append(self)

    def draw(self):
        angle = self.body.angle
        img = pygame.transform.rotate(self.img, math.degrees(angle))
        x, y = convert_coordinates(self.body.position)
        window.blit(img, (x-self.r, y-self.r))
        # x, y = convert_coordinates(self.body.position)
        # window.blit(self.img, (x-self.r, y-self.r))

    def get_shape(self):
        return self.shape


# TYPE == 1
class Pig(Circle):
    img = pygame.image.load(PATH+"pig.png")
    img = pygame.transform.scale(img, (64, 64))

    def __init__(self, position):
        super().__init__(position, type=1)


# TYPE == 2
class Bird(Circle):
    img = pygame.image.load(PATH+"bird.png")

    def __init__(self, position):
        super().__init__(position, type=2)


# TYPE == 0
class Rectangle:
    def __init__(self, position, type=0):
        mass = 10.0
        size = self.img.get_size()
        moment = pymunk.moment_for_box(mass, (size[0], size[1]))
        self.body = pymunk.Body(mass, moment)
        self.body.position = position
        self.shape = pymunk.Poly.create_box(self.body, size)
        # self.shape.density = 1
        # self.shape.elasticity = 0
        self.shape.friction = 0.4
        self.shape.collision_type = type
        space.add(self.body, self.shape)
        Game.objects.append(self)

    def draw(self):
        angle = self.body.angle
        img = pygame.transform.rotate(self.img, math.degrees(angle))
        size = img.get_size()
        x, y = convert_coordinates(self.body.position)
        window.blit(img, (x-size[0]//2, y-size[1]//2))


class Row(Rectangle):
    img = pygame.image.load(PATH+"row.png")

    def __init__(self, position):
        super().__init__(position)


class Column(Rectangle):
    img = pygame.image.load(PATH+"column.png")

    def __init__(self, position):
        super().__init__(position)


class Game:
    objects = []
    score = 0

    def __init__(self):
        self.running = True
        self.background_img = pygame.image.load(PATH+"background.png")
        self.slingshot_img = pygame.image.load(PATH+"slingshot.png")
        self.slingshot_img = pygame.transform.scale(self.slingshot_img, (35, 100))
        self.slingshot2_img = pygame.image.load(PATH+"slingshot2.png")
        self.slingshot2_img = pygame.transform.scale(self.slingshot2_img, (35, 50))
        self.bird_img = pygame.image.load(PATH+"bird.png")
        self.level = 1
        self.set_level(1)
        self.font = pygame.font.SysFont("comicsans", 30, True)
        self.active_shape = None
        self.pulling = False
        self.pulling_pos = None

    def run(self):
        while self.running:
            for event in pygame.event.get():
                self.do_events(event)

            self.draw()

        pygame.quit()

    def do_events(self, event):
        if event.type == QUIT:
            self.running = False

        if event.type == KEYDOWN:
            if event.key >= K_1 and event.key <= K_4:
                level = int(event.unicode)
                self.set_level(level)

        elif event.type == MOUSEBUTTONDOWN:
            position = convert_coordinates(event.pos)
            self.active_shape = None
            for shape in space.shapes:
                l = shape.point_query(position)
                dist = l.distance
                if dist < 0 and isinstance(l.shape, pymunk.shapes.Circle) == True and l.shape.body.body_type == pymunk.Body.STATIC:
                    self.pulling = True
                    self.active_shape = shape
                    self.pulling_pos = self.active_shape.body.position

        elif event.type == MOUSEMOTION:
            if self.pulling:
                self.pulling_pos = convert_coordinates(event.pos)

        elif event.type == MOUSEBUTTONUP:
            if self.pulling:
                self.pulling = False

                position = self.active_shape.body.position
                for object in Game.objects:
                    if isinstance(object, Bird):
                        Game.objects.pop(Game.objects.index(object))
                        break
                
                space.remove(self.active_shape)
                space.remove(self.active_shape.body)
                bird = Bird(position)
                self.active_shape = bird.get_shape()

                body = self.active_shape.body
                p0 = Vec2d(body.position[0], body.position[1])
                p1 = convert_coordinates(event.pos)
                pp = p0-p1
                impulse = 70 * Vec2d(pp[0], pp[1]).rotated(-1*body.angle)
                body.apply_impulse_at_local_point(impulse)
                self.active_shape = None

    def draw(self):
        # Set background
        window.blit(self.background_img, (0, 0))

        # Draw text level and score
        text = self.font.render(f"level {self.level} - score {Game.score}", 1, (0,0,0))
        window.blit(text, (950, 10))

        for object in Game.objects:
            object.draw()

        # pygame.draw.line(window, (0,0,0), convert_coordinates((0, 10)), convert_coordinates((1200, 10)), 5) # comment
        self.draw_slingshot()
        self.draw_active_shape()
        pygame.display.update()
        clock.tick(FPS)
        space.step(1/FPS)

    def set_level(self, level):
        if self.level != level:
            Game.score = 0
        self.level = level
        self.remove_objects()
        self.set_ground()
        self.set_slingshot()

        if self.level == 1:
            Column((1000, 55))
            Column((1060, 55))
            Row((1030, 110))
            Column((1000, 165))
            Column((1060, 165))
            Row((1030, 210))
            Pig((1140, 60))

        elif self.level == 2:
            for i in range(2):
                Column((1000, 60 + i*100))
                Column((1083, 60 + i*100))
                Row((1040, 105 + i*100))
            Pig((1040, 50))

        elif self.level == 3:
            for i in range(790, 1090, 30):
                Column((i, 60))
            Pig((1140, 60))

        elif level == 4:
            Column((790, 60))
            for i in range(790, 990, 85):
                Column((i+83, 60))
                Row((i+40, 105))
                Pig((i+40, 50))

            Pig((1140, 60))

    def remove_objects(self):
        Game.objects = []
        for body in space.bodies:
            space.remove(body)
        
        for shape in space.shapes:
            space.remove(shape)


    def set_ground(self):
        body = pymunk.Body(body_type=pymunk.Body.STATIC)
        shape = pymunk.Segment(body, (0, 10), (1200, 10), 5)
        shape.elasticity = 0.8
        shape.friction = 1
        shape.collision_type = 9
        space.add(body, shape)
        
    def set_slingshot(self):
        body = pymunk.Body(body_type=pymunk.Body.STATIC)
        shape = pymunk.Segment(body, (200, 10), (200, 40), 5)
        shape.elasticity = 1
        shape.friction = 0.3
        space.add(body, shape)

        size = self.slingshot2_img.get_size()
        bird = Bird((200-size[0]//2-10, 55+size[1]//2))
        bird.body.body_type=pymunk.Body.STATIC

    def draw_slingshot(self):
        # pygame.draw.line(window, (0,0,0), convert_coordinates((200, 10)), convert_coordinates((200, 40)), 5) # comment
        x, y = convert_coordinates((200, 55))
        size = self.slingshot_img.get_size()
        window.blit(self.slingshot_img, (x-size[0]//2, y-size[1]//2))
        size = self.slingshot2_img.get_size()
        x, y = convert_coordinates((200, 55+size[1]//2))
        window.blit(self.slingshot2_img, (x-18-size[1]//2, y+7-size[1]//2))

    def draw_active_shape(self):
        if self.active_shape !=None:
            body = self.active_shape.body
            p0 = convert_coordinates(body.position)

            if self.pulling:
                pygame.draw.circle(window, (255, 0, 0), p0, self.active_shape.radius, 3)
                pygame.draw.line(window, (255, 0, 0), p0, convert_coordinates(self.pulling_pos), 3)
                pygame.draw.circle(window, (255, 0, 0), convert_coordinates(self.pulling_pos), self.active_shape.radius, 3)



if(__name__ == "__main__"):
    game = Game()
    game.run()

